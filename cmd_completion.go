package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

func newCompletion(pather CommandPather) *cobra.Command {
	var flags struct {
		shell string
	}
	cmd := &cobra.Command{
		Use:   "completion",
		Short: "Generates shell completion scripts",
		Long: fmt.Sprintf(`Outputs the autocomplete configuration for some shells.

For example, you can add autocompletion for your current bash session using:

    . <( %[1]s completion )

To permanently add bash autocompletion, run:

    %[1]s completion > /etc/bash_completion.d/%[1]s
`, pather.CommandPath()),
		RunE: func(cmd *cobra.Command, args []string) error {
			cmd.SilenceUsage = true
			switch flags.shell {
			case "bash":
				return cmd.Root().GenBashCompletion(os.Stdout)
			case "zsh":
				return cmd.Root().GenZshCompletion(os.Stdout)
			case "fish":
				return cmd.Root().GenFishCompletion(os.Stdout, true)
			default:
				return fmt.Errorf("unknown shell: %s", flags.shell)
			}
		},
	}
	cmd.Flags().StringVar(&flags.shell, "shell", "bash", "Shell type (bash|zsh|fish)")
	return cmd
}
