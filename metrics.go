package main

import "github.com/prometheus/client_golang/prometheus"

const (
	statusSuccess = "success"
	statusFailure = "failure"
)

var (
	tokenLookupDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "idp_lookup_duration_seconds",
			Help:    "IdP token lookup and verification latency distributions. It includes cache lookup times.",
			Buckets: []float64{.005, .01, .025, .05, .1, .25, .5, 1, 2.5, 5, 10},
		},
		[]string{"method", "status"},
	)

	tokenCacheHits = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "idp_lookup_cache_total",
			Help: "IdP token lookup cache hits/misses.",
		},
		[]string{"tenant", "username", "result"},
	)

	cdnRequests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "cdn_requests_total",
			Help: "HTTP requests on the CDN front.",
		},
		[]string{"cdn_type", "tenant", "k8s_cluster_name", "host", "status"},
	)

	cdnBytesSent = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name: "cdn_bytes_sent",
			Help: "The size of response in bytes, excluding HTTP headers.",
		},
		[]string{"cdn_type", "tenant", "k8s_cluster_name", "host", "status", "method", "zone"},
	)

	cdnDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "cdn_request_duration_seconds",
			Help: "The request processing time in milliseconds",
		},
		[]string{"cdn_type", "tenant", "k8s_cluster_name", "host", "method", "normalized_cache_status"},
	)

	cdnRTT = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "cdn_rtt_seconds",
			Help: "The TCP stack's smoothed round trip time (RTT) estimate, in microseconds.",
		},
		[]string{"cdn_type", "tenant", "k8s_cluster_name", "host", "method"},
	)

	cacheRequests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "cdn_cache_requests_total",
			Help: "Cache requests on the CDN front.",
		},
		[]string{"cdn_type", "tenant", "k8s_cluster_name", "host", "cache_status", "normalized_cache_status", "zone"},
	)

	proxyRequestSize = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name: "logql_http_request_size_bytes",
			Help: "Request size going through the logql proxy.",
		},
		[]string{"tenant"},
	)

	wafTraffic = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "waf_traffic",
			Help: "WAF traffic by result",
		},
		[]string{"cdn_type", "tenant", "k8s_cluster_name", "pop", "host", "result"},
	)
)

func init() {
	prometheus.MustRegister(tokenLookupDuration)
	prometheus.MustRegister(tokenCacheHits)
	prometheus.MustRegister(cdnRequests)
	prometheus.MustRegister(cdnBytesSent)
	prometheus.MustRegister(cdnDuration)
	prometheus.MustRegister(cdnRTT)
	prometheus.MustRegister(cacheRequests)
	prometheus.MustRegister(proxyRequestSize)
	prometheus.MustRegister(wafTraffic)
}
