package main

import (
	"bytes"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

var (
	defaultOAuthTimeout   = time.Second * 15
	headerResultingTenant = "X-Resulting-Tenant"
)

func newProxy(upstream string) (http.Handler, error) {
	remote, err := url.Parse(upstream)
	if err != nil {
		return nil, err
	}
	path := remote.EscapedPath()
	remote.Path = ""

	errlog := logrus.StandardLogger().WriterLevel(logrus.ErrorLevel)
	proxy := httputil.NewSingleHostReverseProxy(remote)
	proxy.ModifyResponse = func(r *http.Response) error {
		r.Header.Del("Server")
		r.Header.Del("X-Powered-By")
		return nil
	}
	proxy.ErrorLog = log.New(errlog, "proxy: ", 0)
	proxy.ErrorHandler = func(w http.ResponseWriter, r *http.Request, err error) {
		logrus.Error(err)
		http.Error(w, "", http.StatusBadGateway)
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Check if path is in scope.
		if !strings.HasPrefix(r.URL.EscapedPath(), path) {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			return
		}
		// Filter all known HTTP API methods.
		if r.Method != http.MethodGet && r.Method != http.MethodPost && r.Method != http.MethodDelete {
			http.Error(w, "", http.StatusMethodNotAllowed)
		}
		if logrus.IsLevelEnabled(logrus.TraceLevel) {
			dump, err := httputil.DumpRequest(r, true)
			if err != nil {
				logrus.Errorf("error dumping incoming request: %v", err)
				http.Error(w, "", http.StatusInternalServerError)
				return
			}
			if _, err := io.Copy(os.Stderr, bytes.NewBuffer(dump)); err != nil {
				logrus.Errorf("error dumping incoming request: %v", err)
				http.Error(w, "", http.StatusInternalServerError)
				return
			}
		}
		proxy.ServeHTTP(w, r)

		// Collect metrics about the request.
		size := r.ContentLength
		tenant := r.Header.Get(headerResultingTenant)
		proxyRequestSize.WithLabelValues(tenant).Observe(float64(size))
	}), nil
}
