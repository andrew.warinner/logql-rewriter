package main

import (
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func newServer(address string, routes http.Handler) (*http.Server, error) {
	mux := http.NewServeMux()
	mux.Handle("/", routes)

	metricsHandler := promhttp.Handler()
	mux.Handle("/metrics", metricsHandler)

	mux.Handle("/healthz", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// 200 Ok.
	}))

	srv := &http.Server{
		Handler:      mux,
		Addr:         address,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
	}

	return srv, nil
}
