package basic

import (
	"context"
	"crypto/sha256"
	"crypto/subtle"
	"encoding/hex"
	"errors"
	"fmt"
	"net/http"
	"sort"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/logql-rewriter/pkg/cache"
	"gitlab.com/mironet/logql-rewriter/pkg/jwt"
	"golang.org/x/oauth2"
	"golang.org/x/time/rate"
)

const (
	defaultPassTokenHeader = "X-Forwarded-Access-Token"
	headerResultingTenant  = "X-Resulting-Tenant"
)

// Endpointer returns concrete OIDC endpoints.
type Endpointer interface {
	Endpoint() oauth2.Endpoint
}

// Verifierer returns an OIDC ID token verifier.
type Verifierer interface {
	Verifier(config *oidc.Config) *oidc.IDTokenVerifier
}

type OIDCProvider interface {
	Endpointer
	Verifierer
}

// Verifier verifies raw id tokens and returns the token.
type Verifier interface {
	Verify(ctx context.Context, rawIDToken string) (*oidc.IDToken, error)
}

// ErrTooManyRequests is returned from Authorize if the rate limiter didn't
// allow the request.
var ErrTooManyRequests error = errors.New("too many requests")

// A counterFunc increases the observed metric by one for the labels given. In
// case of Prometheus the labels should match of course. Labels are tenant,
// username, result (in this order).
type counterFunc func(labels ...string)

// Authorizer authorizes requests based on HTTP Basic authentication.
type Authorizer struct {
	headerName string

	provider        OIDCProvider
	oauthConfig     *oauth2.Config
	verifier        Verifier
	cache           *cache.Request
	cacheCounter    counterFunc
	passTokenHeader string
}

// Authorize checks if the current request is authorized to use a specific
// tenant name in the HTTP header named `l.headerName`.
func (au *Authorizer) Authorize(ctx context.Context, r *http.Request, limit *rate.Limiter) error {
	// Fetch header content.
	hdr := r.Header.Get(au.headerName)
	if hdr == "" {
		logrus.Debugf("header %s is empty, trying to authenticate and add the header", au.headerName)
	}
	// Get token from IdP and check header if authorized for forwarding.
	username, password, ok := r.BasicAuth()
	if !ok {
		return fmt.Errorf("no basic auth credentials provided")
	}

	var cacheResult = cache.ResultMiss
	var tenant string
	defer func() {
		if au.cacheCounter != nil {
			au.cacheCounter(tenant, username, cacheResult)
		}
		if tenant != "" {
			r.Header.Add(headerResultingTenant, tenant)
		}
	}()

	// Hash the username and the header value as a key to the cache for later
	// lookup.
	kk := sha256.Sum256([]byte(username + ":" + hdr + ":" + r.Method))
	key := hex.EncodeToString(kk[:])
	pwhash := sha256.Sum256([]byte(password))
	if cred, ok := au.cache.Get(key); ok {
		// Check if the password matches the one in our cache.
		if subtle.ConstantTimeCompare(cred.Hash, pwhash[:]) == 1 {
			logrus.Debugf("credential cache hit for %s", username)
			// Passwords match, cache hit.
			if au.passTokenHeader != "" {
				r.Header.Set(au.passTokenHeader, string(cred.Token))
			}
			if cred.Tenant != "" {
				r.Header.Set(au.headerName, cred.Tenant)
				tenant = cred.Tenant
			}
			cacheResult = cache.ResultHit
			return nil
		}
		// We invalidate the cache. Perhaps the user has a new password.
		au.cache.Del(key)
	}
	if limit != nil && !limit.Allow() {
		cacheResult = cache.ResultCancelled
		return ErrTooManyRequests
	}
	// Now we fetch a new token and check the claims against the header.
	logrus.Debugf("requesting token from IdP: %s", au.provider.Endpoint().TokenURL)
	token, err := au.oauthConfig.PasswordCredentialsToken(ctx, username, password)
	if err != nil {
		return fmt.Errorf("could not get tokens from IdP: %w", err)
	}
	accessToken, err := jwt.ParseJWT(token.AccessToken)
	if err != nil {
		return err
	}
	if err := jwt.AuthorizeMethod(accessToken, r.Method, au.oauthConfig.ClientID); err != nil {
		return fmt.Errorf("could not authorize request: %w", err)
	}
	logrus.Debugf("method %s authorized for user %s", r.Method, username)
	rawIDToken, ok := token.Extra("id_token").(string)
	if !ok {
		return fmt.Errorf("no id_token field in oauth2 token")
	}
	idToken, err := au.verifier.Verify(ctx, rawIDToken)
	if err != nil {
		return fmt.Errorf("error verifying id token: %w", err)
	}
	var claims struct {
		Tenant  string   `json:"tenant"`
		Tenants []string `json:"tenants,omitempty"` // If the id token contains multiple tenants, we check all of them.
	}
	if err := idToken.Claims(&claims); err != nil {
		return fmt.Errorf("could not parse id token tenant claim: %w", err)
	}
	tenantList := append(claims.Tenants, claims.Tenant)
	logrus.Debugf("got valid id token from IdP, allowed tenants: %v", tenantList)
	if hdr == "" && claims.Tenant != "" {
		// If the header was not set on the incoming request, we just set it
		// here.
		hdr = claims.Tenant
		r.Header.Set(au.headerName, hdr) // Set the header if it was unset before.
	}
	if hdr == "" {
		// If the header was empty and we got a list of valid tenants, we can't
		// set the header, thus giving up here.
		return fmt.Errorf("header %s was empty and we got more than one tenant from the IdP: %v", au.headerName, tenantList)
	}
	if !containsString(tenantList, hdr) {
		return fmt.Errorf("tenant %s not in authorized tenant list", hdr)
	}
	// Successful authentication and authorization; cache it.
	au.cache.Put(key, cache.Credential{
		Hash:      pwhash[:],
		Token:     []byte(rawIDToken),
		ExpiresAt: token.Expiry,
		Tenant:    hdr,
	})
	tenant = hdr
	// Pass the token to upstream as a default, if set.
	if au.passTokenHeader != "" {
		r.Header.Set(au.passTokenHeader, rawIDToken)
	}
	return nil
}

func containsString(list []string, s string) bool {
	for _, v := range list {
		if v == s {
			return true
		}
	}
	return false
}

// New returns a new authorizer.
func New(opts ...Opt) (*Authorizer, error) {
	re := new(Authorizer)
	re.cache = cache.New()
	re.passTokenHeader = defaultPassTokenHeader

	for _, opt := range opts {
		if err := opt(re); err != nil {
			return nil, err
		}
	}

	return re, nil
}

// Opt configures this authorizer.
type Opt func(*Authorizer) error

// WithHeaderName sets the header name whose content we authorize.
func WithHeaderName(name string) Opt {
	return func(a *Authorizer) error {
		a.headerName = name
		return nil
	}
}

// WithOIDCProvider provied an OIDC provider to check authn against.
func WithOIDCProvider(provider *oidc.Provider) Opt {
	return func(a *Authorizer) error {
		if provider == nil {
			return fmt.Errorf("provider can't be nil")
		}
		a.provider = provider
		return nil
	}
}

// WithOAuth2Config sets the configuration for the OIDC endpoint.
func WithOAuth2Config(clientID, clientSecret string, scopes []string) Opt {
	return func(a *Authorizer) error {
		if a.provider == nil {
			return fmt.Errorf("provider should be set before setting oauth config")
		}
		scopes = append(scopes, oidc.ScopeOpenID)
		sort.Strings(scopes)
		j := 0
		for i := 1; i < len(scopes); i++ {
			if scopes[j] == scopes[i] {
				continue // Deduplicate.
			}
			j++
			scopes[j] = scopes[i]
		}
		scopes = scopes[:j+1] // We can be sure at least one element is present.
		conf := oauth2.Config{
			ClientID:     clientID,
			ClientSecret: clientSecret,
			// Endpoints are auto-discovered by the provider.
			Endpoint: a.provider.Endpoint(),
			Scopes:   scopes,
		}
		a.oauthConfig = &conf
		a.verifier = a.provider.Verifier(&oidc.Config{ClientID: clientID})
		return nil
	}
}

// WithTokenHeader sets the header name of the passed id token.
func WithTokenHeader(header string) Opt {
	return func(a *Authorizer) error {
		a.passTokenHeader = header
		return nil
	}
}

// WithCacheCounter sets a func which is executed with a set of labels on cache
// hit/miss.
func WithCacheCounter(f counterFunc) Opt {
	return func(a *Authorizer) error {
		a.cacheCounter = f
		return nil
	}
}
