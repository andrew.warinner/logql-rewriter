package fastly

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"testing"

	"gitlab.com/mironet/logql-rewriter/pkg/loki"

	"github.com/go-test/deep"
)

func fixture(name string, t *testing.T) []byte {
	data, err := os.ReadFile("testdata/" + name)
	if err != nil {
		t.Fatal(err)
	}
	return data
}

func init() {
	insideTest = true
}

func TestFastly2loki(t *testing.T) {
	type wantFunc func(*loki.LokiLogLine) error
	type args struct {
		jsonSrc io.Reader
		tenant  string
	}
	tests := []struct {
		name    string
		args    args
		want    wantFunc
		wantErr bool
	}{
		{
			name: "valid",
			args: args{
				jsonSrc: bytes.NewReader(fixture("fastly_log_line.json", t)),
				tenant:  "goofy",
			},
			want: func(lll *loki.LokiLogLine) error {
				want := fixture("fastly_log_line_result.json", t)
				enc, err := json.Marshal(lll)
				if err != nil {
					return err
				}
				if diff := deep.Equal(string(want), string(enc)); diff != nil {
					return fmt.Errorf("%+v", diff)
				}
				return nil
			},
			wantErr: false,
		},
		{
			name: "empty string",
			args: args{
				jsonSrc: bytes.NewBufferString(""),
				tenant:  "",
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := fastly2Loki(tt.args.jsonSrc, tt.args.tenant, nil, nil, nil, nil, nil)
			if (err != nil) != tt.wantErr {
				t.Errorf("Fastly2loki() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.want != nil {
				if err := tt.want(got); err != nil {
					t.Errorf("Fastly2loki() error = %v, got xxxx= %s", err, got)
				}
			}
		})
	}
}
