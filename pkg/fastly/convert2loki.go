package fastly

import (
	"encoding/json"
	"fmt"
	"io"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/mironet/logql-rewriter/pkg/loki"
)

const (
	cacheHit     = "HIT"
	cacheMiss    = "MISS"
	cachePass    = "PASS"
	cacheUnknown = "UNKNOWN"
)

const (
	wafPassed  = "PASSED"
	wafBlocked = "BLOCKED"
	wafLogged  = "LOGGED"
	wafFailure = "FAILURE"
)

// FastlyLogLine is something fastly sends us. This has to be configured in the
// dashboard (service). See https://docs.fastly.com/en/guides/custom-log-formats
// for explanation of what we're using.
type FastlyLogLine struct {
	Host        string `json:"host"`               // %{if(req.http.Fastly-Orig-Host, req.http.Fastly-Orig-Host, req.http.Host)}V
	IP          string `json:"ip"`                 // %a
	Date        string `json:"date"`               // %t
	Path        string `json:"path"`               // %{json.escape(req.url.path)}V
	Method      string `json:"method"`             // %{json.escape(req.method)}V
	Protocol    string `json:"protocol"`           // %{json.escape(req.proto)}V
	Status      int    `json:"status"`             // %{resp.status}V
	Response    string `json:"response,omitempty"` // %{json.escape(resp.response)}V
	Bytes       int    `json:"bytes"`              // %B
	CacheStatus string `json:"cache_status"`       // %{json.escape(fastly_info.state)}V
	RequestTime int64  `json:"request_time"`       // %{time.elapsed.msec}V <-- Milliseconds ⚠
	Zone        string `json:"zone"`               // %{json.escape(server.datacenter)}V
	RoundTrip   int64  `json:"round_trip"`         // %{client.socket.tcpi_rtt}V
	ClusterName string `json:"cluster_name"`       // Fixed value, pre-provisioned in fastly because fastly can't know the k8s cluster name.
}

type FastlyWebLogLine struct {
	Host                 string `json:"host"` // %{if(req.http.Fastly-Orig-Host, req.http.Fastly-Orig-Host, req.http.Host)}V
	Type                 string `json:"type"`
	ServiceID            string `json:"service_id"`
	RequestID            string `json:"request_id"`
	StartTime            int64  `json:"start_time"` // %{time.start.msec}V <-- Milliseconds ⚠
	FastlyInfo           string `json:"fastly_info"`
	Referer              string `json:"referer"`
	Datacenter           string `json:"datacenter"`
	ClientIP             string `json:"client_ip"`
	ReqMethod            string `json:"req_method"`
	ReqURI               string `json:"req_uri"`
	ReqHHost             string `json:"req_h_host"`
	ReqHUserAgent        string `json:"req_h_user_agent"`
	ReqHAcceptEncoding   string `json:"req_h_accept_encoding"`
	ReqHeaderBytes       int    `json:"req_header_bytes"`
	ReqBodyBytes         int    `json:"req_body_bytes"`
	WafLogged            int    `json:"waf_logged"`
	WafBlocked           int    `json:"waf_blocked"`
	WafFailures          int    `json:"waf_failures"`
	WafExecuted          int    `json:"waf_executed"`
	AnomalyScore         int    `json:"anomaly_score"`
	SQLInjectionScore    int    `json:"sql_injection_score"`
	RfiScore             int    `json:"rfi_score"`
	LfiScore             int    `json:"lfi_score"`
	RceScore             int    `json:"rce_score"`
	PhpInjectionScore    int    `json:"php_injection_score"`
	SessionFixationScore int    `json:"session_fixation_score"`
	HTTPViolationScore   int    `json:"http_violation_score"`
	XSSScore             int    `json:"xss_score"`
	RespStatus           int    `json:"resp_status"`
	RespBytes            int    `json:"resp_bytes"`
	RespHeaderBytes      int    `json:"resp_header_bytes"`
	RespBodyBytes        int    `json:"resp_body_bytes"`
	ClusterName          string `json:"cluster_name"` // Fixed value, pre-provisioned in fastly because fastly can't know the k8s cluster name.
}

type FastlyWafLogLine struct {
	Host         string `json:"host"`                // %{if(req.http.Fastly-Orig-Host, req.http.Fastly-Orig-Host, req.http.Host)}V
	ClientIP     string `json:"client_ip,omitempty"` // %a
	Path         string `json:"path,omitempty"`      // %{json.escape(req.url.path)}V
	Type         string `json:"type"`
	WafBlocked   int    `json:"waf_blocked,omitempty"`
	RequestID    string `json:"request_id"`
	RuleID       int    `json:"rule_id"`
	Severity     int    `json:"severity"`
	AnomalyScore int    `json:"anomaly_score"`
	Logdata      string `json:"logdata"`
	WafMessage   string `json:"waf_message"`
	ClusterName  string `json:"cluster_name"` // Fixed value, pre-provisioned in fastly because fastly can't know the k8s cluster name.
}

const cdnType = "fastly"

var (
	insideTest bool
	testNow    = time.Date(2006, 02, 01, 03, 04, 05, 0, time.UTC)
)

func fastlyWebLog2Loki(in io.Reader, tenant string, wafTraffic *prometheus.CounterVec) (*loki.LokiLogLine, error) {
	fastlyLog := make([]FastlyWebLogLine, 0)
	if err := json.NewDecoder(in).Decode(&fastlyLog); err != nil {
		return nil, fmt.Errorf("could not parse fastly log: %w", err)
	}

	streams := make(map[string][][]string)
	now := time.Now().UnixNano()
	if insideTest {
		now = testNow.UnixNano()
	}
	for _, v := range fastlyLog {
		enc, err := json.Marshal(v)
		if err != nil {
			return nil, fmt.Errorf("could not marshal fastly WAF web log line to json: %w", err)
		}
		entry := []string{strconv.FormatInt(now, 10), string(enc)}
		streams[v.Host] = append(streams[v.Host], entry)

		if wafTraffic != nil {
			switch {
			case v.WafBlocked == 1:
				wafTraffic.WithLabelValues(cdnType, tenant, v.ClusterName, v.Datacenter, v.Host, wafBlocked).Inc()
			case v.WafLogged == 1:
				wafTraffic.WithLabelValues(cdnType, tenant, v.ClusterName, v.Datacenter, v.Host, wafLogged).Inc()
			case v.WafFailures > 0:
				wafTraffic.WithLabelValues(cdnType, tenant, v.ClusterName, v.Datacenter, v.Host, wafFailure).Inc()
			default:
				wafTraffic.WithLabelValues(cdnType, tenant, v.ClusterName, v.Datacenter, v.Host, wafPassed).Inc()
			}
		}
	}

	return toLokiLogLine(streams, tenant), nil
}

func fastlyWafLog2Loki(in io.Reader, tenant string) (*loki.LokiLogLine, error) {
	fastlyLog := make([]FastlyWafLogLine, 0)
	if err := json.NewDecoder(in).Decode(&fastlyLog); err != nil {
		return nil, fmt.Errorf("could not parse fastly log: %w", err)
	}

	streams := make(map[string][][]string)
	now := time.Now().UnixNano()
	if insideTest {
		now = testNow.UnixNano()
	}
	for _, v := range fastlyLog {
		enc, err := json.Marshal(v)
		if err != nil {
			return nil, fmt.Errorf("could not marshal fastly WAF web log line to json: %w", err)
		}
		entry := []string{strconv.FormatInt(now, 10), string(enc)}
		streams[v.Host] = append(streams[v.Host], entry)
	}
	return toLokiLogLine(streams, tenant), nil

}

func fastly2Loki(in io.Reader, tenant string,
	counter *prometheus.CounterVec,
	sizeCounter *prometheus.SummaryVec,
	durationCounter *prometheus.HistogramVec,
	rttCounter *prometheus.HistogramVec,
	cacheCounter *prometheus.CounterVec,
) (*loki.LokiLogLine, error) {
	fastlyLog := make([]FastlyLogLine, 0)
	if err := json.NewDecoder(in).Decode(&fastlyLog); err != nil {
		return nil, fmt.Errorf("could not parse fastly log: %w", err)
	}

	// We use this map to group by host.
	streams := make(map[string][][]string)

	// Keep the same timestamp for all log entries we received in one single
	// request. This is not a stream so we should be fine.
	//
	// Docs stating: > logs sent to Loki for every stream must be in
	// timestamp-ascending order; logs with identical timestamps are only
	// allowed if their content differs.
	//
	// https://grafana.com/docs/loki/latest/api/#post-lokiapiv1push
	now := time.Now().UnixNano()
	if insideTest {
		now = testNow.UnixNano()
	}
	for _, v := range fastlyLog {
		// Count requests.
		if counter != nil {
			counter.WithLabelValues(cdnType, tenant, v.ClusterName, v.Host, strconv.Itoa(v.Status)).Inc()
		}
		if sizeCounter != nil {
			sizeCounter.WithLabelValues(cdnType, tenant, v.ClusterName, v.Host, strconv.Itoa(v.Status), v.Method, v.Zone).Observe(float64(v.Bytes))
		}
		if durationCounter != nil {
			// Convert milliseconds to seconds (float).
			d := time.Duration(v.RequestTime) * time.Millisecond
			s := float64(d) / float64(time.Second)
			durationCounter.WithLabelValues(cdnType, tenant, v.ClusterName, v.Host, v.Method, normalizeFastlyCacheStatus(v.CacheStatus)).Observe(s)
		}
		if rttCounter != nil {
			// Convert microseconds to seconds (float).
			d := time.Duration(v.RoundTrip) * time.Microsecond
			s := float64(d) / float64(time.Second)
			rttCounter.WithLabelValues(cdnType, tenant, v.ClusterName, v.Host, v.Method).Observe(s)
		}
		if cacheCounter != nil {
			cacheCounter.WithLabelValues(cdnType, tenant, v.ClusterName, v.Host, v.CacheStatus, normalizeFastlyCacheStatus(v.CacheStatus), v.Zone).Inc()
		}
		enc, err := json.Marshal(v)
		if err != nil {
			return nil, fmt.Errorf("could not marshal fastly log line to json: %w", err)
		}
		entry := []string{strconv.FormatInt(now, 10), string(enc)}
		streams[v.Host] = append(streams[v.Host], entry)
	}

	return toLokiLogLine(streams, tenant), nil
}

func toLokiLogLine(streams map[string][][]string, tenant string) *loki.LokiLogLine {
	lokiLog := new(loki.LokiLogLine)
	// Make sure output is deterministic.
	keys := make([]string, 0)
	for k := range streams {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		stream := loki.LokiStream{
			Stream: loki.LokiLabels{
				Tenant: tenant,
				Host:   k,
			},
			Values: streams[k],
		}
		lokiLog.Streams = append(lokiLog.Streams, stream)
	}
	return lokiLog
}

func normalizeFastlyCacheStatus(status string) string {
	// Our "normal" cache statuses are:
	// HIT
	// MISS
	// PASS
	//
	// Fastly cache statuses are mapped onto normal statuses:
	// HIT, HITPASS, HIT-STALE, HIT-SYNTH -> HIT
	// MISS -> MISS
	// PASS -> PASS
	// NONE -> UNKNOWN
	//
	// N.B. Fastly cache statuses can be modified by suffixes (-CLUSTER, -REFRESH, -WAIT).
	//
	// Fastly cache states per https://developer.fastly.com/reference/vcl/variables/miscellaneous/fastly-info-state/
	// NONE	No state assigned.
	// HIT	The request has matched previously-cached content as a result of a cache lookup.
	// HITPASS	The cache key associated with this request has been previously marked uncacheable in vcl_fetch, which will trigger a PASS and prevent the resulting backend response from being cached. Never a final state.
	// HIT-STALE	The request has matched previously-cached, stale content as a result of a backend failure or a return(deliver_stale) statement.
	// HIT-SYNTH	The request has transited vcl_error as a result of a network error during a backend fetch or an explicit error statement, and a synthetic response has been created with a non-error status code.
	// MISS	The request has been or will be sent to a backend server as a result of failing to find suitable content in cache.
	// PASS	The request has been or will be sent to a backend server as a result of an explicit return(pass) or because a hit-for-pass object was found in cache.
	// Additional suffixes applied to status:
	// -CLUSTER	The request has been transferred to a second cache node due to clustering.
	// -REFRESH	A backend fetch returned a 304 Not Modified response, and Fastly is therefore able to serve cached content that would otherwise be considered stale.
	// -WAIT	This request was collapsed with other requests and placed in a queue because a compatible request was already in progress.
	if strings.HasPrefix(status, "HIT") {
		return cacheHit
	} else if strings.HasPrefix(status, "MISS") {
		return cacheMiss
	} else if strings.HasPrefix(status, "PASS") {
		return cachePass
	} else {
		return cacheUnknown
	}
}
