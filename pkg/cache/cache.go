package cache

import (
	"sync"
	"time"
)

const (
	ResultHit       = "hit"
	ResultMiss      = "miss"
	ResultCancelled = "cancelled"
)

// Credential is what we cache from user's requests.
type Credential struct {
	Hash      []byte    // The user's password hash.
	Token     []byte    // ID token in case we want to pass it upstream.
	ExpiresAt time.Time // Time when the cache is going to expire.
	Tenant    string    // If not empty, pass it to the header name upstream.
}

// Request is a very simple cache implementation to prevent issuing too many
// requests to the IdP.
type Request struct {
	mu sync.Mutex
	m  map[string]Credential
}

// New returns a new credential cache.
func New() *Request {
	cache := &Request{
		m: make(map[string]Credential),
	}
	return cache
}

// Put credentials into the cache.
func (c *Request) Put(key string, cred Credential) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.m[key] = cred
	// Start goroutine to remove it after t expired. Entries are not supposed to
	// be updated, therefore we don't care about resetting the timer.
	go func() {
		time.AfterFunc(time.Until(cred.ExpiresAt), func() {
			c.mu.Lock()
			defer c.mu.Unlock()
			delete(c.m, key)
		})
	}()
}

// Get credential entry.
func (c *Request) Get(key string) (Credential, bool) {
	c.mu.Lock()
	defer c.mu.Unlock()
	cred, ok := c.m[key]
	return cred, ok
}

// Del credential entry.
func (c *Request) Del(key string) bool {
	c.mu.Lock()
	defer c.mu.Unlock()
	_, ok := c.m[key]
	if ok {
		delete(c.m, key)
	}
	return ok
}
