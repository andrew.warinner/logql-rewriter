package tencent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"

	"github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
)

const (
	// This is the log receiving path fixed for tencent.
	logPath = "/tencent"
)

type Handler struct {
	mux             http.Handler // All requests go through here first.
	headerName      string       // Tenant header name.
	upstreamPushAPI *url.URL     // Upstream push API to rewrite requests to.
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.mux.ServeHTTP(w, r)
}

type Opt func(*Handler) error

// WithHeaderName sets the tenant header name.
func WithHeaderName(name string) Opt {
	return func(h *Handler) error {
		h.headerName = name
		return nil
	}
}

// WithUpstream sets the upstream Push API of the target Loki server where all
// push requests are being sent to.
func WithUpstream(upstream string) Opt {
	return func(h *Handler) error {
		up, err := url.Parse(upstream)
		if err != nil {
			return fmt.Errorf("could not parse URL for upstream API: %w", err)
		}

		h.upstreamPushAPI = up
		return nil
	}
}

func NewHandler(next http.Handler, opts ...Opt) (http.Handler, error) {
	h := &Handler{}

	for _, opt := range opts {
		if err := opt(h); err != nil {
			return nil, err
		}
	}

	// Setup routing.
	mux := mux.NewRouter()
	mux.HandleFunc(logPath, h.handleLog(next)).Methods(http.MethodPost) // Chain proxy request.

	// Catch-all.
	mux.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r) // Chain to next handler.
	})

	h.mux = mux

	return h, nil
}

// The incoming Tencent log structure is parsed and transformed to a loki log
// line which then is forwarded upstream.
func (h *Handler) handleLog(next http.Handler) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		tenant := r.Header.Get(h.headerName)
		if tenant == "" {
			logrus.Error("tenant header empty")
			http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

			return
		}

		line, err := tencent2Loki(r.Body, tenant)
		if err != nil {
			logrus.Errorf("could not parse incoming tencent log line: %v", err)
			http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

			return
		}

		// Attach new request body and update the Content-Length header.
		enc, err := json.Marshal(line)
		if err != nil {
			logrus.Errorf("could not encode loki log line: %v", err)
			http.Error(rw, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

			return
		}

		body := bytes.NewReader(enc)
		r.Body = ioutil.NopCloser(body)
		r.ContentLength = body.Size()

		if _, ok := r.Header["Content-Length"]; ok {
			r.Header.Set("Content-Length", strconv.FormatInt(body.Size(), 10))
		}

		// Rewrite the path in the request URL to use Loki's push API.
		u := new(url.URL)
		u.Path = h.upstreamPushAPI.Path
		*r.URL = *u
		r.RequestURI = u.RequestURI()

		if logrus.IsLevelEnabled(logrus.TraceLevel) {
			dump, err := httputil.DumpRequest(r, true)
			if err != nil {
				logrus.Errorf("could not dump outgoing request: %v", err)
				http.Error(rw, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

				return
			}

			logrus.Tracef("%s\n", dump)
		}

		next.ServeHTTP(rw, r)
	}
}
