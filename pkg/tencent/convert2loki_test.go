package tencent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"testing"

	"github.com/go-test/deep"
	"gitlab.com/mironet/logql-rewriter/pkg/loki"
)

func fixture(t *testing.T, name string) []byte {
	t.Helper()

	data, err := ioutil.ReadFile("testdata/" + name)
	if err != nil {
		t.Fatal(err)
	}

	return data
}

func init() {
	insideTest = true
}

func TestTencent2Loki(t *testing.T) {
	type wantFunc func(*loki.LokiLogLine) error

	type args struct {
		jsonSrc io.Reader
		tenant  string
	}

	tests := []struct {
		name    string
		args    args
		want    wantFunc
		wantErr bool
	}{
		{
			name: "valid",
			args: args{
				jsonSrc: bytes.NewReader(fixture(t, "tencent_log_line.json")),
				tenant:  "goofy",
			},
			want: func(lll *loki.LokiLogLine) error {
				want := fixture(t, "tencent_log_line_result.json")
				enc, err := json.Marshal(lll)
				if err != nil {
					return err
				}
				if diff := deep.Equal(string(want), string(enc)); diff != nil {
					return fmt.Errorf("%+v", diff)
				}
				return nil
			},
			wantErr: false,
		},
		{
			name: "empty string",
			args: args{
				jsonSrc: bytes.NewBufferString(""),
				tenant:  "",
			},
			want:    nil,
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tencent2Loki(tt.args.jsonSrc, tt.args.tenant)
			if (err != nil) != tt.wantErr {
				t.Errorf("tencent2loki() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.want != nil {
				if err := tt.want(got); err != nil {
					t.Errorf("tencent2loki() error = %v", err)
				}
			}
		})
	}
}
