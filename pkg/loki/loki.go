package loki

type LokiLabels struct {
	Tenant string `json:"tenant"`
	Host   string `json:"host"`
}

type LokiStream struct {
	Stream LokiLabels `json:"stream"`
	Values [][]string `json:"values"`
}

type LokiLogLine struct {
	Streams []LokiStream `json:"streams"`
}
