package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// CommandPather returns the path to a command.
type CommandPather interface {
	CommandPath() string
}

// initViper works around issue #233 in viper:
// https://github.com/spf13/viper/issues/233
func initViper(v *viper.Viper) {
	v.SetEnvPrefix("LOGQL")
	v.AutomaticEnv()
	v.SetEnvKeyReplacer(strings.NewReplacer("-", "_", ".", "_"))
}

func main() {
	cmd := &cobra.Command{
		Use:           "logql-rewriter",
		Short:         "logql-rewriter authorizes logql HTTP API requests.",
		SilenceErrors: true,
	}
	cmd.AddCommand(
		newCompletion(cmd),
		newVersion(cmd),
		newServe(cmd),
	)
	if err := cmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		os.Exit(1)
	}
}
