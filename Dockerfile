# syntax=docker/dockerfile:1.0-experimental
# Accept the Go version for the image to be set as a build argument.
# Default to Go 1.16
ARG GO_VERSION=1.19

# First stage: build the executable.
FROM golang:${GO_VERSION}-alpine AS builder

# Create the user and group files that will be used in the running container to
# run the process as an unprivileged user.
RUN mkdir /user && \
    echo 'nobody:x:65534:65534:nobody:/:' > /user/passwd && \
    echo 'nobody:x:65534:' > /user/group

# Install the Certificate-Authority certificates for the app to be able to make
# calls to HTTPS endpoints.
# Git is required for fetching the dependencies.
RUN apk add --no-cache ca-certificates git tzdata zip

# Set the environment variables for the go command:
# * CGO_ENABLED=0 to build a statically-linked executable
# * GOFLAGS=-mod=vendor to force `go build` to look into the `/vendor` folder.
ENV CGO_ENABLED=0

# Set up the time zone data so go can use time.Location
WORKDIR /usr/share/zoneinfo
# -0 means no compression.  Needed because go's
# tz loader doesn't handle compressed data.
RUN zip -r -0 /zoneinfo.zip .

# Set the working directory outside $GOPATH to enable the support for modules.
WORKDIR /src

# # Fetch dependencies first; they are less susceptible to change on every build
# # and will therefore be cached for speeding up the next build.
COPY ./go.mod ./go.sum ./
RUN go mod download
# RUN time go build -installsuffix 'static'
# RUN rm main.go

# Import the whole code from the context.
COPY ./ ./

ARG APP_VERSION=unversioned

RUN export | grep -i app_version

# Build the executable to `/app`. Mark the build as statically linked.
RUN --mount=type=cache,target=/root/.cache/go-build \
    go build \
    #-x \
    #-mod=vendor \
    -installsuffix 'static' \
    -ldflags "-X main.version=$APP_VERSION" \
    -o /app

# Final stage: the running container.
FROM busybox:1.32.0 AS final

# Import the user and group files from the first stage.
COPY --from=builder /user/group /user/passwd /etc/

# Import the Certificate-Authority certificates for enabling HTTPS.
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# Import the compiled executable from the first stage.
COPY --from=builder /app /app

# Import the time zone data
COPY --from=builder /zoneinfo.zip /

# Tell go where to finde the time zone data
ENV ZONEINFO /zoneinfo.zip

# Declare the port on which the webserver will be exposed.
# As we're going to run the executable as an unprivileged user, we can't bind
# to ports below 1024.
EXPOSE 5353

# Perform any further action as an unprivileged user.
USER nobody:nobody

ENTRYPOINT [ "/app" ]
