package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/mironet/logql-rewriter/pkg/auth/basic"
	"golang.org/x/time/rate"
)

const (
	authorizationHeaderName   = "Authorization"
	proxyAuthMethodHeaderName = "X-Proxy-Auth-Method"
	methodBasic               = "basic"
	methodJWT                 = "jwt"
)

type authorizer interface {
	Authorize(ctx context.Context, r *http.Request, limit *rate.Limiter) error
}

type switchingAuthorizer struct {
	basic              authorizer
	jwt                authorizer
	removeTokenHeaders []string
	limit              *rate.Limiter
	next               http.Handler // Handoff to this handler if successful (possibly the proxy).
}

func newSwitchingAuthorizer(basic, jwt authorizer, headers []string, limit *rate.Limiter, next http.Handler) http.Handler {
	s := &switchingAuthorizer{
		basic:              basic,
		jwt:                jwt,
		removeTokenHeaders: headers,
		limit:              limit,
		next:               next,
	}
	return s
}

// Prove switchingAuthorizer is an http.Handler.
var _ http.Handler = new(switchingAuthorizer)

func (s switchingAuthorizer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(r.Context(), defaultOAuthTimeout)
	defer cancel()
	r = r.WithContext(ctx) // Make a shallow copy of the request.
	if err := s.authorize(ctx, r, s.limit); err != nil {
		logrus.Error(err)
		if errors.Is(err, basic.ErrTooManyRequests) {
			http.Error(w, http.StatusText(http.StatusTooManyRequests), http.StatusTooManyRequests)
			return
		}
		http.Error(w, "", http.StatusForbidden)
		return
	}
	s.next.ServeHTTP(w, r) // Continue in case of success.
}

// authorize switches based an authentication method.
func (s switchingAuthorizer) authorize(ctx context.Context, r *http.Request, lim *rate.Limiter) error {
	var (
		status = statusFailure
		method string
	)
	// Collect duration metrics.
	start := time.Now()
	defer func() {
		d := time.Since(start)
		tokenLookupDuration.WithLabelValues(method, status).Observe(d.Seconds())
	}()

	defer func() {
		// Remove the auth header from the original request if asked to do so.
		for _, v := range s.removeTokenHeaders {
			logrus.Debugf("removing header %s from call to upstream", v)
			delete(r.Header, v)
		}
	}()

	// Check if we use basic auth or JWT auth.
	_, _, ok := r.BasicAuth()
	if ok {
		logrus.Debugf("using basic authentication")
		method = methodBasic
		// Use basic auth.
		r.Header.Set(proxyAuthMethodHeaderName, "basic")
		if err := s.basic.Authorize(ctx, r, lim); err != nil {
			return err
		}
		status = statusSuccess
		return nil
	}

	// Else try JWT auth.
	if bearer := r.Header.Get(authorizationHeaderName); strings.HasPrefix(bearer, "Bearer ") {
		logrus.Debugf("using JWT authentication")
		method = methodJWT
		r.Header.Set(proxyAuthMethodHeaderName, "jwt")
		if err := s.jwt.Authorize(ctx, r, lim); err != nil {
			return err
		}
		status = statusSuccess
		return nil
	}

	return fmt.Errorf("no valid auth method detected")
}
